/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.edurandc.conditional;

/**
 *
 * @author eduardo durand <eduardo.durand.c@uni.pe>
 */
public class App {

    public static void main(String[] args) {
        System.out.println("Conditional statements");

        int i = 35;
        if (30 < i) {
            System.out.println("el número es mayor a 30");
        }
        if (i % 2 == 1) {
            System.out.println("Es impar");
        }
        if (i % 2 == 0) {
            System.out.println("Es par");
        }
        
        System.out.println("--------------------------");

        int j = 31;
        if (30 < j) {
            System.out.println("el número es mayor a 30");
        } else {

            if (j % 2 == 1) {
                System.out.println("Es impar");
            } else {

                if (j % 2 == 0) {
                    System.out.println("Es par");
                }
            }
        }
       /*
        System.out.println("+++++++++++++");
        int k = 31;
        if (30 < k) {
            System.out.println("el número es mayor a 30");
            return;
        }
        if (k % 2 == 1) {
            System.out.println("Es impar");
            return;
        }
        if (k % 2 == 0) {
            System.out.println("Es par");
            return; //si no es el ultimo del codigo no está de mas de lo contrario si lo está
        }
        */
       
       int m = 12;
       switch(i%2) {
           case 0:
               System.out.println("Es par");
               break; //ya no evalua las otras si es verdadera
           case 1:
               System.out.println("Es impar");
               break;
           default:
               System.out.println("Cualquier otro número");
       }
    }
}
