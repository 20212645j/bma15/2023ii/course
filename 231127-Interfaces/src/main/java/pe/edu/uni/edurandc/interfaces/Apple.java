/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package pe.edu.uni.edurandc.interfaces;

/**
 *
 * @author Andre Durand <eduardo.durand.c@uni.pe>
 */
public class Apple extends Fruit {

    @Override
    public String howToEat() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        return "Apple: make a Struddle!";
    }

}
