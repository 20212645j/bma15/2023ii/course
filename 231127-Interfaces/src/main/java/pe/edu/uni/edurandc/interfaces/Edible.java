/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package pe.edu.uni.edurandc.interfaces;

/**
 *
 * @author Andre Durand <eduardo.durand.c@uni.pe>
 */
public interface Edible {
    public abstract String howToEat();
}
